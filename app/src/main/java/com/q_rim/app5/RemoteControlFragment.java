package com.q_rim.app5;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class RemoteControlFragment extends Fragment {
  private TextView selectedTextView, workingTextView;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_remote_control, parent, false);

    this.selectedTextView = (TextView)v.findViewById(R.id.fragment_remote_control_selected_TextView);
    this.workingTextView = (TextView)v.findViewById(R.id.fragment_remote_control_working_TextView);
    View.OnClickListener numberButtonListener = new View.OnClickListener() {
      public void onClick(View v) {
        TextView textView = (TextView)v;
        String working = workingTextView.getText().toString();
        String text = textView.getText().toString();  // get current text from textView
        if (working.equals("0")) {                    // initial display value is 0
          workingTextView.setText(text);
        } else {
          workingTextView.setText(working + text);    // initial display value is not 0
        }
      }
    };

    // Wiring up the number buttons.  Writing the number on the buttons via code. <24.9>
    TableLayout tableLayout = (TableLayout)v.findViewById(R.id.fragment_remote_control_tableLayout);
    int number = 1;
    for (int i = 2; i < tableLayout.getChildCount() - 1; i++) {
      TableRow row = (TableRow)tableLayout.getChildAt(i);
      for (int j = 0; j < row.getChildCount(); j++) {
        Log.d("------", ""+number);
        Button button = (Button)row.getChildAt(j);
        button.setText(""+number);
        button.setOnClickListener(numberButtonListener);
        number++;
      }
    }

    // Wiring up the last row <24.10>
    TableRow bottomRow = (TableRow)tableLayout.getChildAt(tableLayout.getChildCount() - 1);

    // Wiring up the last row:  Delete Button <24.10>
    Button deleteButton = (Button)bottomRow.getChildAt(0);
    deleteButton.setText("Delete");
    deleteButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        workingTextView.setText("0");
      }
    });

    // Wiring up the last row:  Zero Button <24.10>
    Button zeroButton = (Button)bottomRow.getChildAt(1);
    zeroButton.setText("0");
    zeroButton.setOnClickListener(numberButtonListener);

    // Wiring up the last row:  Enter Button <24.10>
    Button enterButton = (Button)bottomRow.getChildAt(2);
    enterButton.setText("Enter");
    enterButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        CharSequence working = workingTextView.getText();
        if (working.length() > 0)
          selectedTextView.setText("0");
      }
    });
    return v;
  }
}
